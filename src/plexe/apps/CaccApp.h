//
// Copyright (C) 2012-2019 Michele Segata <segata@ccs-labs.org>
// Copyright (C) 2018-2019 Julian Heinovski <julian.heinovski@ccs-labs.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef CACCAPP_H_
#define CACCAPP_H_

#include <algorithm>
#include <memory>

#include "plexe/apps/BaseApp.h"

namespace plexe {
    class CaccApp : public BaseApp {

        public:
            CaccApp() {};
            ~CaccApp();

            /** override from BaseApp */
            virtual void initialize(int stage) override;
            void setHasCacc(bool newHasCacc);

        protected:
            /** override this method of BaseApp. we want to handle it ourself */
            virtual void handleLowerMsg(cMessage* msg) override;

            /**
             * Handles PlatoonBeacons
             *
             * @param PlatooningBeacon pb to handle
             */
            virtual void onPlatoonBeacon(const PlatooningBeacon* pb) override;

            /** convenience methods **/
            void changeController(int newController);

            virtual void handleSelfMsg(cMessage* msg);

        private:
            cMessage* checkLeader;
            bool hasCacc;

            void doCheckLeader();
    };
} // namespace plexe

#endif
