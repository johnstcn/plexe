//
// Copyright (C) 2012-2019 Michele Segata <segata@ccs-labs.org>
// Copyright (C) 2018-2019 Julian Heinovski <julian.heinovski@ccs-labs.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "plexe/apps/CaccApp.h"
#include "plexe/messages/PlatooningBeacon_m.h"
#include "plexe/protocols/BaseProtocol.h"
#include "veins/modules/messages/BaseFrame1609_4_m.h"
#include "veins/modules/utility/Consts80211p.h"

#define CACCAPP_CHECK_LEADER_INTERVAL 1000

using namespace veins;

namespace plexe {

    Define_Module(CaccApp);

    void CaccApp::initialize(int stage)
    {
        BaseApp::initialize(stage);
        hasCacc = false;

        if (stage == 0) return;

        // stage == 1
        checkLeader = new cMessage("checkLeader");
        SimTime rounded = SimTime(floor(simTime().dbl() * 1000 + CACCAPP_CHECK_LEADER_INTERVAL), SIMTIME_MS);
        scheduleAt(rounded, checkLeader);
    }

    void CaccApp::setHasCacc(bool newHasCacc)
    {
        hasCacc = newHasCacc;
    }

    void CaccApp::handleLowerMsg(cMessage* msg)
    {
        BaseFrame1609_4* unicast = check_and_cast<BaseFrame1609_4*>(msg);

        cPacket* enc = unicast->getEncapsulatedPacket();
        ASSERT2(enc, "received a UnicastMessage with nothing inside");

        if (enc->getKind() == BaseProtocol::BEACON_TYPE) {
            onPlatoonBeacon(check_and_cast<PlatooningBeacon*>(enc));
        }

        delete(unicast);
    }

    void CaccApp::onPlatoonBeacon(const PlatooningBeacon* pb)
    {
        // XXX: hard-coding things is bad
        if (!hasCacc) {
            EV_INFO << "vehicle " << myId << " is not a CAV, ignoring beacon";
            return;
        }

        auto theirID = pb->getVehicleId();
        // EV_INFO << "my id: " << myId << endl;
        // EV_INFO << "sender id: " << theirID << endl;
        // positionHelper->dumpVehicleData();

        auto myLaneIdx = traciVehicle->getLaneIndex();
        auto theirLaneIdx = pb->getLaneIdx();
        if (myLaneIdx != theirLaneIdx) {
            // EV_INFO << "vehicle " << myId << " says " << theirID << "not in my lane" << endl;
            return;
        }

        auto myRoadID = traciVehicle->getRoadId().c_str();
        auto theirRoadID = pb->getRoadID();
        if (strcmp(myRoadID, theirRoadID) != 0) {
            // EV_INFO << "vehicle " << myId << " says " << theirID << "not on my road" << endl;
            return;
        }

        auto myAngle = traciVehicle->getAngle();
        myAngle -= 90; // TODO: why does getAngle() return 90 for right-facing?
        auto theirAngle = pb->getAngle();
        if (abs(myAngle - theirAngle) > 90) {
            // EV_INFO << "vehicle " << myId << " says " << theirID << "not in my direction" << endl;
            return;
        }

        // TODO: arbitrary fixed value
        const float threshold = 50;
        // XXX: using getLeader() here is a total cop-out.
        std::pair<std::string, double> leader = traciVehicle->getLeader(threshold);
        std::string sLeaderID = std::get<0>(leader);
        int leaderID;
        if (strcmp(sLeaderID.c_str(), "") == 0) {
            leaderID = -1;
        } else {
            auto dot_idx = sLeaderID.find_last_of(".");
            auto nLeaderID = sLeaderID.substr(dot_idx+1, sLeaderID.size());
            leaderID = std::stoi(nLeaderID);
        }
        double distance = std::get<1>(leader);
        // EV_INFO << "vehicle " << myId << " getLeader() says leader=" << leaderID << " distance=" << distance << std::endl;
        int theirVehicleID = pb->getVehicleId();
        if (leaderID != theirVehicleID) {
            // EV_INFO << "vehicle " << myId << " says " << theirID << " not in front of me" << std::endl;
            return;
        }

        if (distance > threshold) {
            // EV_INFO << "vehicle " << myId << " says " << theirID << " is more than " << threshold << "m away" << std::endl;
            return;
        }

        // OK, we have a message from a vehicle in front of us and within reasonable range.
        // Let's engage CACC and make it our "leader".
        plexeTraciVehicle->setFrontVehicleData(
            pb->getControllerAcceleration(),
            pb->getAcceleration(),
            pb->getSpeed(),
            pb->getPositionX(),
            pb->getPositionY(),
            pb->getTime()
        );

        plexeTraciVehicle->setLeaderVehicleData(
            pb->getControllerAcceleration(),
            pb->getAcceleration(),
            pb->getSpeed(),
            pb->getPositionX(),
            pb->getPositionY(),
            pb->getTime()
        );
        // set our desired speed to the leading vehicle's speed
        plexeTraciVehicle->setCruiseControlDesiredSpeed(pb->getSpeed());
        changeController(CACC);
        // this is local only
        positionHelper->setPlatoonFormation({myId, theirVehicleID});
    }

    void CaccApp::changeController(int newController) {
        auto activeController = plexeTraciVehicle->getActiveController();
        if (activeController == newController) {
            return;
        }
        // EV_INFO << "vehicle " << myId << " activeController " << activeController << " -> " << newController << endl;
        plexeTraciVehicle->setActiveController(newController);
    }

    void CaccApp::handleSelfMsg(cMessage* msg)
    {
        BaseApp::handleSelfMsg(msg);
        if (msg == checkLeader) {
            // XXX: hard-coding things is bad
                if (!hasCacc) {
                EV_INFO << "vehicle " << myId << " is not a CAV, ignoring checkLeader selfMsg";
                return;
            }
            doCheckLeader();
            scheduleAt(simTime() + SimTime(CACCAPP_CHECK_LEADER_INTERVAL, SIMTIME_MS), checkLeader);
        }
    }

    void CaccApp::doCheckLeader()
    {
        auto f = positionHelper->getPlatoonFormation();
        if (f.size() < 2) {
            changeController(DRIVER);
            return;
        }
        
        VEHICLE_DATA data;
        plexeTraciVehicle->getVehicleData(&data);
        const double threshold = 1000;
        double age = SimTime().dbl() - data.time;
        if (age < threshold) {
            return;
        }
        // EV_INFO << "vehicle " << myId << " has not heard from its leader in more than " << threshold << "ms, disengaging CACC" << std::endl;
    }

    CaccApp::~CaccApp() {
        cancelAndDelete(checkLeader);
    }
}