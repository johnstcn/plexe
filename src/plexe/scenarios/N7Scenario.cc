//
// Copyright (C) 2018-2019 Julian Heinovski <julian.heinovski@ccs-labs.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "plexe/scenarios/N7Scenario.h"

using namespace veins;

namespace plexe {

Define_Module(N7Scenario);

const int PLATOON_LANE = 0;

void N7Scenario::initialize(int stage)
{
    BaseScenario::initialize(stage);

    if (stage == 0) {
        // get pointer to application
        appl = FindModule<CaccApp*>::findSubModule(getParentModule());
        caccVTypes = strset(par("caccVTypes").stdstringValue(), ',');
    }

    if (stage == 1) {
        std::string myVType = traciVehicle->getVType();
        if (caccVTypes.find(myVType) != caccVTypes.end()) {
            appl->setHasCacc(true);
            EV_INFO << "N7Scenario init " << myVType << " ACC";
            plexeTraciVehicle->setActiveController(ACC);
            plexeTraciVehicle->setCruiseControlDesiredSpeed(100.0 / 3.6);
        } else {
            EV_INFO << "N7Scenario init " << myVType << " DRIVER";
            plexeTraciVehicle->setActiveController(DRIVER);
        }
    }
}

N7Scenario::~N7Scenario()
{
}

void N7Scenario::handleSelfMsg(cMessage* msg) {
    BaseScenario::handleSelfMsg(msg);
}

// helper function
std::set<std::string> N7Scenario::strset(const std::string& s, const char& delim)
{
    std::string buf{""};
    std::set<std::string> ret;

    for (char c : s) {
        if (c != delim) {
            buf.push_back(c);
        } else if (c == delim && !buf.empty()) {
            ret.insert(buf);
            buf.clear();
        }
    }
    if (!buf.empty()) ret.insert(buf);
    return ret;
}

} // namespace plexe
