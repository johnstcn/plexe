//
// Copyright (C) 2018-2019 Julian Heinovski <julian.heinovski@ccs-labs.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef N7SCENARIO_H_
#define N7SCENARIO_H_

#include "plexe/scenarios/BaseScenario.h"
#include "plexe/apps/CaccApp.h"

namespace plexe {

class N7Scenario : public BaseScenario {
public:
    // Destructor
    virtual ~N7Scenario();
    // Override from BaseScenario
    virtual void initialize(int stage) override;

protected:
    // application layer
    CaccApp* appl;

    std::set<std::string> caccVTypes;

    // Override from BaseScenario
    virtual void handleSelfMsg(cMessage* msg) override;

public:
    // Constructor
    N7Scenario() {
        appl = nullptr;
    };

private:
    //utility function
    std::set<std::string> strset(const std::string& s, const char& delim);
};

} // namespace plexe

#endif
