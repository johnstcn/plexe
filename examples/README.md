# Scenarios

The following scenarios were added as part of my Masters thesis:

* `cacc`: a simple example scenario for testing the non-platooning CACC module.
* `gdn7`: Full N7 Scenario imported from [Guériau & Dusparic, 2020](https://github.com/maxime-gueriau/ITSC2020_CAV_impact).
* `gdn7_full_6am8am`: 2-hour subset of `gdn7` scenario - this was used for final results.
* `gdn7_small`: subset of `gdn7` scenario used for testing the non-platooning CACC on a realistic road network.
* `gdn7_small_baseline`: modified version of `gdn7_small` to act as a baseline comparison between HDVs and CAVs.
* `platooning_comparison`: modified version of original `platooning` scenario to investigate performance differences of various CACC controllers w.r.t. packet drop rate (PDR) on a simple road network.

The remaining scenarios are the original scenarios included with PLEXE:

* `autolanechange`
* `engine`
* `human`
* `joinManeuver`
* `platooning`
