=== ADDED BY CIAN JOHNSTON, 2020-09-14 ===

Changes were made to this project to support the dissertation titled "Realistic Simulation of Cooperative Adaptive Cruise Control (CACC) Degradation under Random Packet Loss".
These changes are detailed in the file "plexe-patch.diff".

All other changes should be attributed to their original authors.


=== ORIGINAL ===

Plexe - The platooning extension for Veins

See the Plexe website <http://plexe.car2x.org/> for a tutorial, documentation,
and publications.

Plexe is composed of many parts. See the version control log for a full list of
contributors and modifications. Each part is protected by its own, individual
copyright(s), but can be redistributed and/or modified under an open source
license. License terms are available at the top of each file. Parts that do not
explicitly include license text shall be assumed to be governed by the "GNU
General Public License" as published by the Free Software Foundation -- either
version 2 of the License, or (at your option) any later version
(SPDX-License-Identifier: GPL-2.0-or-later). Parts that are not source code and
do not include license text shall be assumed to allow the Creative Commons
"Attribution-ShareAlike 4.0 International License" as an additional option
(SPDX-License-Identifier: GPL-2.0-or-later OR CC-BY-SA-4.0). Full license texts
are available with the source distribution.

Besides myself (Michele Segata), Plexe has been improved over the years thanks
to some incredibly valuable contributors:

Bastian Bloessl
Tobias Hardes
Julian Heinovski
Stefan Joerer
Max Schettler
Christoph Sommer
